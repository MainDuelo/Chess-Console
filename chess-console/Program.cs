﻿using System;
using board;
using chess;

namespace chess_console
{
    class Program
    {
        static void Main(string[] args)
        { 
            try
            {
                ChessGame match = new ChessGame();
                while (!match.finished)
                {
                    try
                    {
                        Console.Clear();
                        Screen.printMatch(match);
                       

                        Console.WriteLine();
                        Console.Write("origin: ");
                        Position origin = Screen.readPositionChess().toPosition();
                        match.validatePositionOrigin(origin);

                        bool[,] psotionPossible = match.board.piece(origin).movementPossibles();

                        Console.Clear();
                        Screen.printBoard(match.board, psotionPossible);

                        Console.WriteLine();
                        Console.Write("destiny: ");
                        Position destiny = Screen.readPositionChess().toPosition();
                        match.validatePositionDestiny(origin, destiny);
                        match.makeMove(origin, destiny);
                    }catch(BoardException e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadLine();
                    }
                }
                Console.Clear();
                Screen.printMatch(match);
                
            }
            catch(BoardException e){
                Console.WriteLine(e.Message);
            }
            Console.ReadLine();
        }
    }
}
