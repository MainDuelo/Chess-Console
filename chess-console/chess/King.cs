﻿using System;
using board;

namespace chess
{
    class King : Piece
    {
        private ChessGame match;
        public King(Board board, Color color, ChessGame match) : base( board, color)
        {
            this.match = match;
        }

        private bool canMove(Position position)
        {
            Piece piece = board.piece(position);
            return piece == null || piece.color !=  this.color;
        }

        private bool verifyTowerRoque(Position position)
        {
            Piece piece = board.piece(position);
            return piece != null && piece is Tower && piece.color == color && piece.numberMovements == 0;
        }

        public void matrix(bool[,] matrix, Position position)
        {
            if (board.validPosition(position) && canMove(position))
            {
                matrix[position.line, position.column] = true;
            }
        }

        public override bool[,] movementPossibles()
        {
            bool[,] matrix = new bool[board.lines, board.columns];
            Position position = new Position(0, 0);
            
            position.setValues(base.position.line - 1, base.position.column);
            this.matrix(matrix, position);
            position.setValues(base.position.line - 1, base.position.column + 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line , base.position.column + 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line + 1, base.position.column + 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line + 1, base.position.column);
            this.matrix(matrix, position);
            position.setValues(base.position.line + 1, base.position.column -1);
            this.matrix(matrix, position);
            position.setValues(base.position.line, base.position.column - 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line - 1, base.position.column - 1);
            this.matrix(matrix, position);
            

            if(numberMovements == 0 && !match.checkMate)
            {
                //# Small Roque
                Position positionTower01 = new Position(base.position.line, base.position.column + 3);

                if (verifyTowerRoque(positionTower01))
                {
                    Position position01 = new Position(base.position.line, base.position.column + 1);
                    Position position02 = new Position(base.position.line, base.position.column + 2);
                    if(board.piece(position01) == null && board.piece(position02) == null)
                    {
                        matrix[base.position.line, base.position.column + 2] = true;
                    }
                }

                //# Big Roque
                Position positionTower02 = new Position(base.position.line, base.position.column - 4);

                if (verifyTowerRoque(positionTower02))
                {
                    Position position01 = new Position(base.position.line, base.position.column - 1);
                    Position position02 = new Position(base.position.line, base.position.column - 2);
                    Position position03 = new Position(base.position.line, base.position.column - 3);
                    if (board.piece(position01) == null && board.piece(position02) == null && board.piece(position03) == null)
                    {
                        matrix[base.position.line, base.position.column - 2] = true;
                    }
                }
            }
            return matrix;
        }

        private void possibleSmallRoque()
        {

        }

        public override string ToString()
        {
            return "K";
        }
    }
}
