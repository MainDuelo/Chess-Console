﻿using System;
using board;

namespace chess
{
    class ChessPosition
    {
        public char column { get; set; }
        public int line { get; set; }
        private int CHESS_SIZE = 8;

        public ChessPosition(char column, int line)
        {
            this.column = column;
            this.line = line;
        }

        public override string ToString()
        {
            return "" + column + line;
        }

        public Position toPosition()
        {
            return new Position(CHESS_SIZE - line, column - 'a');
        }
    }
}
