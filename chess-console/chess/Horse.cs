﻿using System;
using board;

namespace chess
{
    class Horse : Piece
    {
        public Horse(Board board, Color color) : base( board, color)
        {

        }

        private bool canMove(Position position)
        {
            Piece piece = board.piece(position);
            return piece == null || piece.color != this.color;
        }

        public void matrix(bool[,] matrix, Position position)
        {
            if (board.validPosition(position) && canMove(position))
            {
                matrix[position.line, position.column] = true;
            }
        }

        public override bool[,] movementPossibles()
        {
            bool[,] matrix = new bool[board.lines, board.columns];
            Position position = new Position(0, 0);

            position.setValues(base.position.line - 1, base.position.column - 2);
            this.matrix(matrix, position);
            position.setValues(base.position.line - 2, base.position.column - 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line - 2, base.position.column + 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line - 1, base.position.column + 2);
            this.matrix(matrix, position);
            position.setValues(base.position.line + 1, base.position.column + 2);
            this.matrix(matrix, position);
            position.setValues(base.position.line + 2, base.position.column + 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line + 2, base.position.column - 1);
            this.matrix(matrix, position);
            position.setValues(base.position.line + 1, base.position.column - 2);
            this.matrix(matrix, position);

            return matrix;
        }

        public override string ToString()
        {
            return "H";
        }
    }
}

