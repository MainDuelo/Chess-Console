﻿using System;
using System.Collections.Generic;
using board;

namespace chess
{
    class ChessGame
    {
        public Board board { get; private set; }
        public int shift { get; private set; }
        public Color currentPlayer { get; private set; }
        public bool finished { get; private set; }
        private HashSet<Piece> pieces;
        private HashSet<Piece> captured;
        public bool checkMate { get; private set; }
        public Piece vulneravelEnPassant { get; private set; }

        public ChessGame()
        {
            board = new Board(8, 8);
            shift = 1;
            currentPlayer = Color.White;
            finished = false;
            checkMate = false;
            pieces = new HashSet<Piece>();
            vulneravelEnPassant = null;
            captured = new HashSet<Piece>();
            putPiece();
        }
        
        public Piece executeMovement(Position origin, Position destiny)
        {
            Piece piece = board.removePiece(origin);
            piece.toIncreaseNumberMovements();
            Piece pieceCaptured = board.removePiece(destiny);
            board.putPiece(piece, destiny);
            if(pieceCaptured != null)
            {
                captured.Add(pieceCaptured);
            }

            executeSmallRoque(piece, origin, destiny);
            executeBigRoque(piece, origin, destiny);
            executeEnPassant(piece, pieceCaptured, origin, destiny);

            return pieceCaptured;
        }

        private void executeSmallRoque(Piece piece, Position origin, Position destiny)
        {
            if (piece is King && destiny.column == origin.column + 2)
            {
                Position originTower = new Position(origin.line, origin.column + 3);
                Position destinyTower = new Position(origin.line, origin.column + 1);
                Piece tower = board.removePiece(originTower);
                tower.toIncreaseNumberMovements();
                board.putPiece(tower, destinyTower);
            }
        }


        private void executeBigRoque(Piece piece, Position origin, Position destiny)
        {
            if (piece is King && destiny.column == origin.column - 2)
            {
                Position originTower = new Position(origin.line, origin.column - 4);
                Position destinyTower = new Position(origin.line, origin.column - 1);
                Piece tower = board.removePiece(originTower);
                tower.toIncreaseNumberMovements();
                board.putPiece(tower, destinyTower);
            }
        }

        private void executeEnPassant(Piece piece, Piece pieceCaptured, Position origin, Position destiny)
        {
            if (piece is Pawn)
            {
                if (origin.column != destiny.column && pieceCaptured == null)
                {
                    Position positionPawn;
                    if (piece.color == Color.White)
                    {
                        positionPawn = new Position(destiny.line + 1, destiny.column);
                    }
                    else
                    {
                        positionPawn = new Position(destiny.line - 1, destiny.column);
                    }
                    pieceCaptured = board.removePiece(positionPawn);
                    captured.Add(pieceCaptured);
                }
            }
        }

        public void undoMovement(Position origin, Position destiny, Piece pieceCaptured)
        {
            Piece piece = board.removePiece(destiny);
            piece.toDecreaseMovements();
            if(pieceCaptured != null)
            {
                board.putPiece(pieceCaptured, destiny);
                captured.Remove(pieceCaptured);
            }
            board.putPiece(piece, origin);

            undoSmallRoque(piece, origin, destiny);
            undoBigRoque(piece, origin, destiny);
            undoEnPassant(piece, pieceCaptured, origin, destiny);

        }
        private void undoSmallRoque(Piece piece, Position origin, Position destiny)
        {
            if (piece is King && destiny.column == origin.column + 2)
            {
                Position originTower = new Position(origin.line, origin.column + 3);
                Position destinyTower = new Position(origin.line, origin.column + 1);
                Piece tower = board.removePiece(destinyTower);
                tower.toDecreaseMovements();
                board.putPiece(tower, originTower);
            }
        }

        private void undoBigRoque(Piece piece, Position origin, Position destiny)
        {
            if (piece is King && destiny.column == origin.column - 2)
            {
                Position originTower = new Position(origin.line, origin.column - 4);
                Position destinyTower = new Position(origin.line, origin.column - 1);
                Piece towr = board.removePiece(destinyTower);
                towr.toDecreaseMovements();
                board.putPiece(towr, originTower);
            }
        }

        private void undoEnPassant(Piece piece, Piece pieceCaptured, Position origin, Position destiny)
        {
            if (piece is Pawn)
            {
                if (origin.column != destiny.column && pieceCaptured == vulneravelEnPassant)
                {
                    Piece pieceRemoved = board.removePiece(destiny);
                    Position positionPawn;
                    if (piece.color == Color.White)
                    {
                        positionPawn = new Position(3, destiny.column);
                    }
                    else
                    {
                        positionPawn = new Position(4, destiny.column);
                    }
                    board.putPiece(pieceRemoved, positionPawn);
                }
            }
        }

        public void makeMove(Position origin, Position destiny)
        {
            Piece pieceCaptured = executeMovement(origin, destiny);

            if (isCheckMate(currentPlayer))
            {
                undoMovement(origin, destiny, pieceCaptured);
                throw new BoardException("You can not put yourself in check!");
            }

            Piece piece = board.piece(destiny);

            promotion(piece, destiny);


            if (isCheckMate(opponent(currentPlayer)))
            {
                checkMate = true;
            }
            else
            {
                checkMate = false;
            }

            if (verifyCheckMate(opponent(currentPlayer)))
            {
                finished = true;
            }
            else {
                shift++;
                changesPlayed();
            }


            makeEnPassant(piece, origin, destiny);
            
            
        }

        private void promotion(Piece piece, Position destiny)
        {
            if (piece is Pawn)
            {
                if ((piece.color == Color.White && destiny.line == 0) || (piece.color == Color.Black && destiny.line == 7))
                {
                    piece = board.removePiece(destiny);
                    pieces.Remove(piece);
                    Piece lady = new Lady(board, piece.color);
                    board.putPiece(lady, destiny);
                    pieces.Add(lady);
                }
            }
        }

        private void makeEnPassant(Piece piece, Position origin, Position destiny)
        {
            if (piece is Pawn && (destiny.line == origin.line - 2 || destiny.line == origin.line + 2))
            {
                vulneravelEnPassant = piece;
            }
            else
            {
                vulneravelEnPassant = null;
            }
        }

        public void validatePositionOrigin(Position position)
        {
            if(board.piece(position) == null)
            {
                throw new BoardException("There is no part in the chosen home position!");
            }

            if(currentPlayer != board.piece(position).color)
            {
                throw new BoardException("The chosen piece of origin is not yours!");
            }

            if (!board.piece(position).existsMovementPossible())
            {
                throw new BoardException("There are no possible moves for the chosen source piece!");
            }
        }

        public void validatePositionDestiny(Position origin, Position destiny)
        {
            if (!board.piece(origin).MovementPossible(destiny))
            {
                throw new BoardException("Target position invalid!");
            }
        }

        private void changesPlayed()
        {
            if(currentPlayer == Color.White)
            {
                currentPlayer = Color.Black;
            }
            else
            {
                currentPlayer = Color.White;
            }
        }

        public HashSet<Piece> pieceCaptured(Color color)
        {
            HashSet<Piece> piece = new HashSet<Piece>();
            foreach(Piece x in captured)
            {
                if(x.color == color)
                {
                    piece.Add(x);
                }
            }
            return piece;
        }

        public HashSet<Piece> pieceInGame(Color color)
        {
            HashSet<Piece> piece = new HashSet<Piece>();
            foreach (Piece x in pieces)
            {
                if (x.color == color)
                {
                    piece.Add(x);
                }
            }
            piece.ExceptWith(pieceCaptured(color));
            return piece;
        }

        private  Color opponent(Color color)
        {
            if(color == Color.White)
            {
                return Color.Black;
            }
            else
            {
                return Color.White;
            }
        }

        private Piece king(Color color)
        {
            foreach(Piece x in pieceInGame(color))
            {
                if(x is King)
                {
                    return x;
                }
            }
            return null;
        }

        public bool isCheckMate(Color color)
        {
            Piece king = this.king(color);
            if(king == null)
            {
                throw new BoardException("Does not have color king" + color + " on the board!");
            }

            foreach(Piece x in pieceInGame(opponent(color)))
            {
                bool[,] matrix = x.movementPossibles();
                if (matrix[king.position.line, king.position.column])
                {
                    return true;
                }    
            }
            return false;
        }

        public bool verifyCheckMate(Color color)
        {
            if (!isCheckMate(color))
            {
                return false;
            }

            foreach(Piece x in pieceInGame(color))
            {
                bool[,] matrix = x.movementPossibles();
                for(int i = 0; i<board.lines; i++)
                {
                    for(int j = 0; j<board.columns; j++)
                    {
                        if (matrix[i, j])
                        {
                            Position origem = x.position;
                            Position destino = new Position(i, j);
                            Piece pecaCapturada = executeMovement(origem, destino);
                            bool testeXeque = isCheckMate(color);
                            undoMovement(origem, destino, pecaCapturada);
                            if (!testeXeque) {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public void putNewPiece(char column, int line, Piece piece)
        {
            board.putPiece(piece, new ChessPosition(column,line).toPosition());
            pieces.Add(piece);
        }
        private void putPiece()
        {
            putNewPiece('a', 1, new Tower(board, Color.White));
            putNewPiece('b', 1, new Horse(board, Color.White));
            putNewPiece('c', 1, new Bishop(board, Color.White));
            putNewPiece('d', 1, new Lady(board, Color.White));
            putNewPiece('e', 1, new King(board, Color.White,this));
            putNewPiece('f', 1, new Bishop(board, Color.White));
            putNewPiece('g', 1, new Horse(board, Color.White));
            putNewPiece('h', 1, new Tower(board, Color.White));
            putNewPiece('a', 2, new Pawn(board, Color.White, this));
            putNewPiece('b', 2, new Pawn(board, Color.White, this));
            putNewPiece('c', 2, new Pawn(board, Color.White, this));
            putNewPiece('d', 2, new Pawn(board, Color.White, this));
            putNewPiece('e', 2, new Pawn(board, Color.White, this));
            putNewPiece('f', 2, new Pawn(board, Color.White, this));
            putNewPiece('g', 2, new Pawn(board, Color.White, this));
            putNewPiece('h', 2, new Pawn(board, Color.White, this));

            putNewPiece('a', 8, new Tower(board, Color.Black));
            putNewPiece('b', 8, new Horse(board, Color.Black));
            putNewPiece('c', 8, new Bishop(board, Color.Black));
            putNewPiece('d', 8, new Lady(board, Color.Black));
            putNewPiece('e', 8, new King(board, Color.Black, this));
            putNewPiece('f', 8, new Bishop(board, Color.Black));
            putNewPiece('g', 8, new Horse(board, Color.Black));
            putNewPiece('h', 8, new Tower(board, Color.Black));
            putNewPiece('a', 7, new Pawn(board, Color.Black, this));
            putNewPiece('b', 7, new Pawn(board, Color.Black, this));
            putNewPiece('c', 7, new Pawn(board, Color.Black, this));
            putNewPiece('d', 7, new Pawn(board, Color.Black, this));
            putNewPiece('e', 7, new Pawn(board, Color.Black, this));
            putNewPiece('f', 7, new Pawn(board, Color.Black, this));
            putNewPiece('g', 7, new Pawn(board, Color.Black, this));
            putNewPiece('h', 7, new Pawn(board, Color.Black, this));
        }  
    }
}
