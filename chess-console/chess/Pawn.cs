﻿using System;
using board;

namespace chess
{
    class Pawn : Piece
    {
        private ChessGame match ;
        public Pawn(Board board, Color color, ChessGame match) : base(board, color)
        {
            this.match = match;
        }

        private bool existsEnemy(Position position)
        {
            Piece piece = board.piece(position);
            return piece != null && piece.color != color;
        }
        private bool free(Position pos)
        {
            return board.piece(pos) == null;
        }
        public void matrix(bool[,] matrix, Position position)
        {
            if (board.validPosition(position) && free(position))
            {
                matrix[position.line, position.column] = true;
            }
        }

        public void matrixInitial(bool[,] matrix, Position position)
        {
            if (board.validPosition(position) && free(position) && numberMovements == 0)
            {
                matrix[position.line, position.column] = true;
            }
        }

        public void matrixCaptured(bool[,] matrix, Position position)
        {
            if (board.validPosition(position) && existsEnemy(position))
            {
                matrix[position.line, position.column] = true;
            }
        }

        public override bool[,] movementPossibles()
        {
            bool[,] matrix = new bool[board.lines, board.columns];
            Position position = new Position(0, 0);

            if(color == Color.White)
            {
                position.setValues(base.position.line - 1, base.position.column );
                this.matrix(matrix, position);
                position.setValues(base.position.line - 2, base.position.column);
                matrixInitial(matrix, position);
                position.setValues(base.position.line - 1, base.position.column - 1);
                matrixCaptured(matrix, position);
                position.setValues(base.position.line - 1, base.position.column + 1);
                matrixCaptured(matrix, position);

                //# En passant

                if(base.position.line == 3)
                {
                    Position left = new Position(base.position.line, base.position.column - 1);
                    if(board.validPosition(left) && existsEnemy(left) && board.piece(left) == match.vulneravelEnPassant)
                    {
                        matrix[left.line -1, left.column] = true;
                    }
                    Position right = new Position(base.position.line, base.position.column + 1);
                    if (board.validPosition(right) && existsEnemy(right) && board.piece(right) == match.vulneravelEnPassant)
                    {
                        matrix[right.line -1, right.column] = true;
                    }

                }
            }
            else
            {
                position.setValues(base.position.line + 1, base.position.column);
                this.matrix(matrix, position);
                position.setValues(base.position.line + 2, base.position.column);
                matrixInitial(matrix, position);
                position.setValues(base.position.line + 1, base.position.column - 1);
                matrixCaptured(matrix, position);
                position.setValues(base.position.line + 1, base.position.column + 1);
                matrixCaptured(matrix, position);

                if (base.position.line == 4)
                {
                    Position left = new Position(base.position.line, base.position.column - 1);
                    if (board.validPosition(left) && existsEnemy(left) && board.piece(left) == match.vulneravelEnPassant)
                    {
                        matrix[left.line + 1, left.column] = true;
                    }
                    Position right = new Position(base.position.line, base.position.column + 1);
                    if (board.validPosition(right) && existsEnemy(right) && board.piece(right) == match.vulneravelEnPassant)
                    {
                        matrix[right.line + 1, right.column] = true;
                    }

                }
            }

            
            return matrix;
        }

        public override string ToString()
        {
            return "P";
        }
    }
}
