﻿using System;
using board;

namespace chess
{
    class Tower : Piece
    {
        public Tower(Board board, Color color) : base(board, color)
        {

        }

        private bool canMove(Position position)
        {
            Piece piece = board.piece(position);
            return piece == null || piece.color != this.color;
        }

        public void matrix(bool[,] matrix, Position position, int direction01, int direction02)
        {
            while (board.validPosition(position) && canMove(position))
            {
                matrix[position.line, position.column] = true;
                if (board.piece(position) != null && board.piece(position).color != color)
                {
                    break;
                }
                position.setValues(position.line + direction01, position.column + direction02);
            }
        }

        public override bool[,] movementPossibles()
        {
            bool[,] matrix = new bool[board.lines, board.columns];
            Position position = new Position(0, 0);

            position.setValues(base.position.line - 1, base.position.column);
            this.matrix(matrix, position, -1, 0);
            position.setValues(base.position.line + 1, base.position.column);
            this.matrix(matrix, position, 1, 0);
            position.setValues(base.position.line, base.position.column + 1);
            this.matrix(matrix, position, 0, 1);
            position.setValues(base.position.line, base.position.column - 1);
            this.matrix(matrix, position, 0, -1);

            return matrix;
        }

        public override string ToString()
        {
            return "T";
        }
    }
}
