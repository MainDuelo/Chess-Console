﻿using System;

namespace board
{
    abstract class Piece
    {
        public Position position { get; set; }
        public Color color { get; protected set; }
        public int numberMovements { get; protected set; }
        public Board board { get; protected set; }

        public Piece(Board board, Color color)
        {
            this.position = null;
            this.board = board;
            this.color = color;
            this.numberMovements = 0;
        }

        public void toIncreaseNumberMovements()
        {
           numberMovements++;
        }

        public void toDecreaseMovements()
        {
            numberMovements--;
        }

        public bool existsMovementPossible()
        {
            bool[,] mat = movementPossibles();
            for (int i = 0; i<board.lines; i++){
                for (int j = 0; j<board.lines; j++){
                    if (mat[i, j]) {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool MovementPossible(Position position)
        {
            return movementPossibles()[position.line, position.column];
        }

        public abstract bool[,] movementPossibles();
    }
}
