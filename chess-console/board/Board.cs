﻿using System;

namespace board
{
    class Board
    {
        public int  lines { get; set; }
        public int columns { get; set; }
        private Piece[,] pieces;

        public Board(int lines, int columns)
        {
            this.lines = lines;
            this.columns = columns;
            pieces = new Piece[lines, columns];
        }

        public Piece piece(int line, int column)
        {
            return pieces[line, column];
        }

        public Piece piece(Position position)
        {
            return pieces[position.line, position.column];
        }

        public void putPiece(Piece piece, Position position)
        {
            if (existsPiece(position))
            {
                throw new BoardException("A piece already exists in this position");
            }
            pieces[position.line, position.column] = piece;
            piece.position = position;
        }
        public bool existsPiece(Position position)
        {
            validatePosition(position);
            return piece(position) != null;
        }


        public Piece removePiece(Position position)
        {
            if(this.piece(position) == null)
            {
                return null;
            }
            Piece piece = this.piece(position);
            piece.position = null;
            pieces[position.line, position.column] = null;
            return piece;
        }

        public bool validPosition(Position position)
        {
            if(position.line<0 || position.line>=lines || position.column<0 || position.column >= columns)
            {
                return false;
            }
            return true;
        }

        public void validatePosition(Position position)
        {
            if (!validPosition(position))
            {
                throw new BoardException("invalid position!");
            }
        }
    }
}
