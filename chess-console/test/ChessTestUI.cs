﻿using System;
using chess;
using board;

namespace chess_console.Test
{
    class ChessTestUI
    {
        public ChessTestUI()
        {
            Console.WriteLine("New Lady");
            newLady();     
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Check");
            checkMate();
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("En Passant");
            enPassant();          
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Roque small and big");
            roque();
            Console.ReadLine();
        }


        private void newLady()
        {
            ChessGame match = new ChessGame();

            Position [] position = setPositionChess("a2", "a4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a7", "a5");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("b2", "b4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a5", "b4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a4", "a5");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("b7", "b6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a5", "a6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("c7", "c6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a6", "a7");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("d7", "d6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a7", "b8");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("e7", "e6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("b8", "b6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            Screen.printMatch(match);

        }

        private void checkMate()
        {
            ChessGame match = new ChessGame();

            Position[] position = setPositionChess("d2", "d4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("c7", "c6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a2", "a4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("d8", "a5");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            Screen.printMatch(match);
        }

        private void enPassant()
        {
            ChessGame match = new ChessGame();

            Position[] position = setPositionChess("h2", "h4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a7", "a6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("h4", "h5");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("g7", "g5");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            Screen.printMatch(match);

            position = setPositionChess("h5", "g6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);
            Screen.printMatch(match);
        }


        private void roque()
        {
            ChessGame match = new ChessGame();

            Position[] position = setPositionChess("a2", "a4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("h7", "h5");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("b2", "b4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("g7", "g5");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("d2", "d4");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("g8", "f6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("b1", "c3");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("f8", "g7");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("c1", "b2");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("a7", "a6");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("d1", "d3");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            Screen.printMatch(match);

            position = setPositionChess("e8", "g8");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            position = setPositionChess("e1", "c1");
            match.validatePositionDestiny(position[0], position[1]);
            match.makeMove(position[0], position[1]);

            Screen.printMatch(match);
        }

        private  Position[] setPositionChess(String positionOrigin, String positionDestiny)
        {
            Position origin = Screen.readPositionChess(positionOrigin).toPosition();
            Position destiny = Screen.readPositionChess(positionDestiny).toPosition();
            Position[] position = { origin, destiny };
            return position;
        }  
    }
}
