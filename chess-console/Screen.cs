﻿using System;
using System.Collections.Generic;
using board;
using chess;

namespace chess_console
{
    class Screen
    {
        public static void  printMatch(ChessGame match)
        {
            printBoard(match.board);
            Console.WriteLine();
            printPieceCaptured(match);
            Console.WriteLine();
            Console.WriteLine("Shift: " + match.shift);

            if (!match.finished)
            {
                Console.WriteLine("Waiting for move: " + match.currentPlayer);
                if (match.checkMate)
                {
                    Console.WriteLine("CHECK!!!");
                }
            }
            else
            {
                Console.WriteLine("CHECK MATE!!!");
                Console.WriteLine("WINNER " + match.currentPlayer);
            }
            
        }

        public static void printPieceCaptured(ChessGame match)
        {
            Console.WriteLine("Piece Captured: ");
            Console.Write("White: ");
            printGroup(match.pieceCaptured(Color.White));
            Console.WriteLine();
            Console.Write("Black: ");
            ConsoleColor consoleColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            printGroup(match.pieceCaptured(Color.Black));
            Console.ForegroundColor = consoleColor;
            Console.WriteLine();

        }
        public static void printGroup(HashSet<Piece> group)
        {
            Console.Write("[");
            foreach(Piece x in group)
            {
                Console.Write(x + " ");
            }
            Console.Write("]");
        }
     
        public static void printBoard(Board board)
        {
            for (int i = 0; i < board.lines; i++)
            {
                Console.Write(8 - i + " ");
                for (int j = 0; j < board.columns; j++)
                {
                    printPiece(board.piece(i, j));
                    
                }
                Console.WriteLine();
            }
            Console.WriteLine("  a b c d e f g h");
        }


        public static void printBoard(Board tab, bool[,] possiblePositions)
        {
            ConsoleColor originalBackground = Console.BackgroundColor;
            ConsoleColor backgroundChanged = ConsoleColor.DarkGray;
            for (int i = 0; i < tab.lines; i++)
            {
                Console.Write(8 - i + " ");
                for (int j = 0; j < tab.columns; j++)
                {
                    if (possiblePositions[i, j])
                    {
                        Console.BackgroundColor = backgroundChanged;
                    }
                    else
                    {
                        Console.BackgroundColor = originalBackground;
                    }
                    printPiece(tab.piece(i, j));
                    Console.BackgroundColor = originalBackground;

                }
                Console.WriteLine();
            }
            Console.WriteLine("  a b c d e f g h");
            Console.BackgroundColor = originalBackground;
        }

        public static ChessPosition readPositionChess()
        {
            string s = Console.ReadLine();
            while (!Screen.validateRead(s))
            {
                Console.Write("Invalid input type again: ");
                s = Console.ReadLine();
            }   
            char column = s[0];
            int line = int.Parse(s[1] + "");
            return new ChessPosition(column, line);
        }

        public static ChessPosition readPositionChess(String position)
        {
            string s = position;
            while (!Screen.validateRead(s))
            {
                Console.Write("Invalid input type again: ");
                s = Console.ReadLine();
            }
            char column = s[0];
            int line = int.Parse(s[1] + "");
            return new ChessPosition(column, line);
        }

        private static bool validateRead(string initials)
        {
            if (initials.Length > 2 || !Char.IsLetter(initials[0]))
            {
                return false;
            }else{
                try
                {
                    char column = initials[0];
                    int line = int.Parse(initials[1] + "");
                }
                catch (Exception e)
                {
                    return false;
                }
            }
 
            return true;
        }

        public static void printPiece(Piece piece)
        {
            if (piece == null)
            {
                Console.Write("- ");
            }
            else
            {
                if (piece.color == Color.White)
                {
                    Console.Write(piece);
                }
                else
                {
                    ConsoleColor aux = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(piece);
                    Console.ForegroundColor = aux;
                }
                Console.Write(" ");
            }
        }
    }
}
